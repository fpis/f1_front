import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import reportWebVitals from "./reportWebVitals";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Dashboard from "./Pages/Dashboard";
import Driver from "./Pages/Driver";
import TopNavbar from "./Components/Dashboard/TopNavbar";
import Footer from "./Components/Dashboard/Footer";
import BookReservation from "./Pages/BookReservation";
import MyReservation from "./Pages/MyReservation";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Dashboard />,
  },
  {
    path: "/driver/:driverId",
    element: <Driver />,
  },
  {
    path: "/book-reservation",
    element: <BookReservation />,
  },
  {
    path: "/my-reservation",
    element: <MyReservation />,
  },
]);

export const sizeNumber = 9;

const queryClient = new QueryClient();

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <div id="page-container">
    <div id="content-wrap">
      <React.StrictMode>
        <QueryClientProvider client={queryClient}>
          <header>
            <TopNavbar />
          </header>
          <RouterProvider router={router} />
          <footer>
            <Footer />
          </footer>
        </QueryClientProvider>
      </React.StrictMode>
    </div>
  </div>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

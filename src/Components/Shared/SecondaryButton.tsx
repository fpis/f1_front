import React from "react";
import "./SecondaryButton.scss";

type Props = {
  button_value: string;
  className?: string;
  width?: string;
  height?: string;
  color?: string;
  background?: string;
  fontSize?: string;
  margin?: string;
  disabled?: () => boolean;
  onClick?: React.MouseEventHandler<HTMLInputElement> | undefined;
};

const SecondaryButton = (props: Props) => {
  const isDisabled = props.disabled ? props.disabled() : false;


  return (
    <div className={`secondary-button ` + props.className}>
      <input
        style={{
          width: props.width !== null ? props.width : "100px",
          height: props.height !== null ? props.height : "50px",
          color: props.color !== null ? props.color : "white",
          background: props.background ? props.background : isDisabled ? "#ba7a78" : "#E10600",
          margin: props.margin !== null ? props.margin : "0px",
          fontSize: props.fontSize !== null ? props.fontSize : "20px",
          textWrap: "wrap",
        }}
        disabled={props.disabled ? props.disabled() : false}
        type="button"
        value={props.button_value}
        onClick={props.onClick}
      />
    </div>
  );
};

export default SecondaryButton;

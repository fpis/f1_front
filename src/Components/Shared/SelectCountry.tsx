import React from "react";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
  Box,
} from "@mui/material";
import { FlagIcon, FlagIconCode } from "react-flag-kit";
import { useGetAllCountries } from "../../Api/Country/CountryApi";
import { CountryModel } from "../../Api/Country/CountryApi.types";

type Props = {
  value: "" | any;
  onChange: (event: SelectChangeEvent<{ value: unknown }>) => void;
  label: string;
  data: CountryModel[];
};

const SelectCountry = ({ value, onChange, label, data }: Props) => {
  
  return (
    <div
      style={{
        marginTop: "10px",
        width: "230px",
      }}
    >
      <FormControl fullWidth sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="country-label">{label}</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={value}
          onChange={onChange}
          variant="standard"
        >
          { data !== undefined?
          data.map((country) => (
            <MenuItem key={country.shortName} value={country.shortName}>
              <Box
                display="flex"
                alignItems="center"
                gap={1}
                textOverflow={"ellipsis"}
              >
                <FlagIcon code={country.shortName} size={20} />
                <span>{country.name}</span>
              </Box>
            </MenuItem>
          )):<div></div>}
        </Select>
      </FormControl>
    </div>
  );
};

export default SelectCountry;

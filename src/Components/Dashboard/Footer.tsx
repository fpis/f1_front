import React from "react";
import "./Footer.scss";
import Logo from "../../Assets/f1_logo.svg";

type Props = {};

const Footer = (props: Props) => {
  return (
    <footer className="footer-wrapper">
      <img src={Logo} alt="logo" />
      <p>@ 2003-2024 Grand Prix de Monaco</p>
    </footer>
  );
};
export default Footer;

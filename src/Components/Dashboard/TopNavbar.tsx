import React from "react";
import "./TopNavbar.scss";
import Logo from "../../Assets/f1_logo.svg";
import Countdown, { CountdownRendererFn } from "react-countdown";

type Props = {};

const Completionist = () => <span>Time for race!</span>;

const renderer: CountdownRendererFn = ({
  days,
  hours,
  minutes,
  seconds,
  completed,
}) => {
  if (completed) {
    return <Completionist />;
  } else {
    return (
      <div className="time-wrapper">
        <div className="time-element">
          {days >= 10 ? days : "0" + days}
          <div className="time-text">Days</div>
        </div>
        <div className="time-element">
          {hours >= 10 ? hours : "0" + hours}
          <div className="time-text">Hours</div>
        </div>
        <div className="time-element">
          {minutes >= 10 ? minutes : "0" + minutes}
          <div className="time-text">Minutes</div>
        </div>
        <div className="time-element">
          {seconds >= 10 ? seconds : "0" + seconds}
          <div className="time-text">Seconds</div>
        </div>
      </div>
    );
  }
};

const TopNavbar = (props: Props) => {
  return (
    <div className="top-navbar">
      <img src={Logo} alt="logo" />
      <ul>
        <li>
          <a href="/">Dashboard</a>
        </li>
        <li>
          <a href="/book-reservation">Book tickets</a>
        </li>
        <li>
          <a href="/my-reservation">My reservations</a>
        </li>
      </ul>
      <Countdown
        date={Date.parse("2024/06/24")}
        renderer={renderer}
      ></Countdown>
    </div>
  );
};

export default TopNavbar;

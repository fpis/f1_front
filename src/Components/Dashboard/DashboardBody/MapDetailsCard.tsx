import React from "react";
import "./MapDetailsCard.scss";

type Props = {
  title: String;
  value: String;
  measureUnit?: String;
};

const MapDetailsCard = (props: Props) => {
  return (
    <div className="map-details-card-wrapper">
      <h5>{props.title}</h5>
      <div className="value-wrapper">
        {props.value.length > 10 ? <h3>{props.value}</h3>:<h1>{props.value}</h1>}
        <h5>{props.measureUnit}</h5>
      </div>
    </div>
  );
};

export default MapDetailsCard;

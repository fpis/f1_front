import React, { useEffect, useState } from "react";
import "./Drivers.scss";
import DriverCard from "./DriverCard";
import Reveal from "../../Shared/Reveal";
import { useGetDriversByGrandPrix } from "../../../Api/Driver/DriverApi";
import { sizeNumber } from "../../..";
import PrimaryButton from "../../Shared/PrimaryButton";
import SecondaryButton from "../../Shared/SecondaryButton";

type Props = {
  grandPrixId: number;
};

const Drivers = (props: Props) => {
  const [page, setPage] = useState<number>(1);
  const {data, refetch, isError, isLoading, isRefetching } = useGetDriversByGrandPrix(page, sizeNumber, props.grandPrixId);

  useEffect(() => {
    refetch();
  }, [page, setPage]);

  return (
    <div className="drivers-wrapper">
      <div className="title">
        <h1>F1 Drivers 2024</h1>
      </div>
      <hr />
      <div className="driver-cards">
        {data !== undefined ?
        data.map((d, i) => (
        <Reveal>
          <DriverCard
            number={i + 1}
            driverId={d.driverId}
            driverCountryShortName={d.countryShortName}
            driverName={d.firstName}
            driverNo={d.number}
            driverPicture={
              d.pictureUrl
            }
            driverPoints={d.thisYearPoints}
            driverSurname={d.lastName}
            driverTeamName={d.teamName}
            color= {d.colorHex}
          />
        </Reveal>
        )): <div></div>  }
      </div>
      <div className="pagination">
        <div>
          {page === 1 ? <></>:<PrimaryButton width="120px" height="40px"button_value="previous" onClick={() => setPage((prev) => prev - 1)}></PrimaryButton>}
        </div>
        <h1>
          Page {page}
        </h1>
        <div>
          {data != null && data.length < sizeNumber  ? <></> : <SecondaryButton width="120px" height="40px" button_value="next" onClick={() => setPage((prev) => prev + 1)}></SecondaryButton>}
        </div>
      </div>
    </div>
  );
};

export default Drivers;

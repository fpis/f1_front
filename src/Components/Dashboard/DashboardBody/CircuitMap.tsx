import React from "react";
import "./CircuitMap.scss";
import MonacoMap from "../../../Assets/monaco-country-flag.png";
import MapDetailsCard from "./MapDetailsCard";
import CircuitMapAvif from "../../../Assets/circuitDeMonaco.avif";
import Reveal from "../../Shared/Reveal";
import RaceInformation from "./RaceInformation";

type Props = {
    additionalInformation: string;
    firstGrandPrix: number;
    numberOfLaps: number;
    circuitLength: number;
    raceDistance: number;
    lapRecord: string;
    lapRecordDriver: string;
};

const CircuitMap = (props: Props) => {
  return (
    <div className="circuit-map-wrapper">
      <div className="title-wrapper">
        <img src={MonacoMap} alt="monacoMap" />
        <h1>Circuit de Monaco</h1>
      </div>
      <div className="map-wrapper">
        <div className="left-map-details">
          <Reveal>
            <MapDetailsCard
              title={"First Grand Prix"}
              value={props.firstGrandPrix != 0? props.firstGrandPrix.toString() : "N/A"}
            />
          </Reveal>
          <Reveal>
            <MapDetailsCard
              title={"Lap record"}
              value={props.lapRecord}
            />
          </Reveal>
          <Reveal>
            <MapDetailsCard
              title={"Lap record owner"}
              value={props.lapRecordDriver}
            />
          </Reveal>
        </div>
        <img src={CircuitMapAvif} alt="" />
        <div className="right-map-details">
          <Reveal>
            <MapDetailsCard
              title={"Number of laps"}
              value={props.numberOfLaps !== 0 ? props.numberOfLaps.toString() : "N/A"}
            />
          </Reveal>
          <Reveal>
            <MapDetailsCard
              title={"Race distance"}
              value={props.raceDistance !== 0 ? props.raceDistance.toString() : "N/A"}
              measureUnit={"km"}
            />
          </Reveal>
          <Reveal>
            <MapDetailsCard
              title={"Circuit length"}
              value={props.circuitLength !== 0 ? props.circuitLength.toString() : "N/A"}
              measureUnit={"km"}
            />
          </Reveal>
        </div>
      </div>
      <hr />
      <RaceInformation 
        additionalInformation={props.additionalInformation}  
      />
    </div>
  );
};

export default CircuitMap;

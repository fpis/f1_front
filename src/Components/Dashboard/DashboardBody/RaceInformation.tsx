import React from "react";
import "./RaceInformation.scss";
import MonacoFlag from "../../../Assets/monaco-flag.png";
import Reveal from "../../Shared/Reveal";

type Props = {
  additionalInformation: string;
};


function returnABC(additionalInformation: string): JSX.Element {
  const mainArray = additionalInformation.slice(additionalInformation.indexOf("\r\n\r\n") + 4, additionalInformation.length).split("\r\n\r\n");
  const elements: JSX.Element[] = [];
  elements.push(<Reveal><h3>{additionalInformation.slice(0, additionalInformation.indexOf("\r\n\r\n"))}</h3></Reveal>)
  mainArray.forEach(item => {
    const additionalArray = item.split("\r\n");
    additionalArray.forEach((line, index) => {
      if (index === 0) {
        elements.push(<Reveal key={index}><h5>{line}</h5></Reveal>);
      } else {
        elements.push(<Reveal key={index}><h3>{line}</h3></Reveal>);
      }
    });
  });

  return <>{elements}</>;
}

const RaceInformation = (props: Props) => {
  return (
    <div className="race-info-wrapper">
      <div className="text-section">
        {returnABC(props.additionalInformation)}
      </div>
      <div className="image-section">
        <img src={MonacoFlag} alt="flag" />
      </div>
    </div>
  );
};

export default RaceInformation;

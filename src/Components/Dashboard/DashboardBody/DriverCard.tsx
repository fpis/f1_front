import React from "react";
import "./DriverCard.scss";
import { useNavigate } from "react-router";
import { FlagIcon, FlagIconCode } from "react-flag-kit";

type Props = {
  number: number;
  driverId: number;
  driverNo: number;
  driverName: string;
  driverSurname: string;
  driverPoints: number;
  driverPicture: string;
  driverTeamName: string;
  driverCountryShortName: FlagIconCode;
  color: string;
};

const DriverCard = (props: Props) => {
  const navigate = useNavigate();

  return (
    <div className="driver-card-wrapper" onClick={() => navigate(`/driver/${props.driverId}`)}>
      <div className="card-top">
        <h1>{props.number.toString()}</h1>
        <div className="card-points">
          <p>{props.driverPoints.toString()}</p>
          <h2>PTS</h2>
        </div>
      </div>
      <hr />
      <div className="card-center">
        <div className="left-wrapper">
          <div className="vertical-line" style={{borderLeft: `6px solid ${props.color}`, height: "40px"}}></div>
          <div className="driver-name">
            <h5>{props.driverName}</h5>
            <h3>{props.driverSurname}</h3>
          </div>
        </div>
        <div className="driver-country">
        <FlagIcon code={props.driverCountryShortName} width={50} height={40} />
        </div>
      </div>
      <hr />
      <div className="card-bottom">
        <h5>{props.driverTeamName}</h5>
        <div className="driver">
          <span id="driver-number" style={{color: `${props.color}`}}>{props.driverNo}</span>
          <img src={props.driverPicture} alt="driver-picture" />
          <div></div>
        </div>
      </div>
    </div>
  );
};

export default DriverCard;

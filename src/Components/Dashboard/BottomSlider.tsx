import React from "react";
import "./BottomSlider.scss";

type Props = {
  bottomSliderSelected: boolean;
  setBottomSliderSelected: React.Dispatch<React.SetStateAction<boolean>>;
};

const BottomSlider = (props: Props) => {
  return (
    <div className="bottom-slider-wrapper">
      <div
        className={
          "bottom-slider-element" +
          (props.bottomSliderSelected ? " active-bar" : " inactive-bar")
        }
      >
        <button
          onClick={() => {
            props.setBottomSliderSelected(true);
          }}
        >
          Circuit
        </button>
      </div>
      <div className="vertical-line"></div>
      <div
        className={
          "bottom-slider-element" +
          (!props.bottomSliderSelected ? " active-bar" : " inactive-bar")
        }
      >
        <button
          onClick={() => {
            props.setBottomSliderSelected(false);
          }}
        >
          Drivers
        </button>
      </div>
    </div>
  );
};

export default BottomSlider;

import React from "react";
import { Fade, Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import HeroImage1 from "../../Assets/image_hero3.jpg";
import HeroImage from "../../Assets/image_hero.jpg";
import "./HeroSlider.scss";
import Number2024 from "../../Assets/number2024.avif";
import Reveal from "../Shared/Reveal";

type Props = {
  grandPrixName: string;
  startDate: Date;
  endDate: Date;
};

const slideImages = [
  {
    url: `${HeroImage1}`,
    caption: "Slide 1",
  },
  {
    url: `${HeroImage}`,
    caption: "Slide 2",
  },
];

const HeroSlider = (props: Props) => {
  return (
    <div className="slide-container">
      <Slide
        arrows={false}
        duration={6000}
        autoplay={true}
        pauseOnHover={false}
        transitionDuration={1400}
        easing="ease-out"
      >
        {slideImages.map((slideImage, index) => (
          <div className="each-slide" key={index}>
            <div
              className="background-image"
              style={{
                filter: "brightness(55%)",
                backgroundImage: `url(${slideImage.url})`,
              }}
            ></div>
          </div>
        ))}
      </Slide>
      <div className="title-hero-background">
        <Reveal>
          <p className="title-hero">
            Grand Prix - {props.grandPrixName.toLocaleUpperCase()}
            <br />
            <img src={Number2024} alt="" height={60} />
            <br />
            <div className="hero-date">{(new Date(props.startDate)).toDateString()} - {(new Date(props.endDate)).toDateString()}</div>
          </p>
        </Reveal>
      </div>
    </div>
  );
};

export default HeroSlider;

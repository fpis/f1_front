import React from "react";
import "./DiscountBanner.scss";
import { RiDiscountPercentFill } from "react-icons/ri";

type Props = {};

const DiscountBanner = (props: Props) => {
  return (
    <div className="discount-banner-wrapper">
      <div className="stripes-wrapper">
        <RiDiscountPercentFill
          color="#E10600"
          size={50}
          style={{
            marginLeft: "5px",
          }}
        />
        <h5>You have a 10% discount on the total price until May 15!</h5>
      </div>
    </div>
  );
};

export default DiscountBanner;

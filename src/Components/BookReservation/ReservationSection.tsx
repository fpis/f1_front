import React, {useState} from "react";
import "./ReservationSection.scss";
import ReservationCard from "./ReservationCard";
import Combobox from "react-widgets/Combobox";
import "react-widgets/styles.css";
import { useGetTicketsFiltered } from "../../Api/Ticket/TicketApi";
import { TicketFiltersModel, TicketModel } from "../../Api/Ticket/TicketApi.types";
import { ReservationItemModel } from "../../Api/Reservation/ReservationApi.types";

type Props = {
  data: TicketModel[] | undefined;
  reservationItems: ReservationItemModel[];
  setReservationItems: React.Dispatch<React.SetStateAction<ReservationItemModel[]>>;
};

function sortBy(value: string, data: TicketModel[] | undefined){
  if(data !== undefined){
    switch(value) {
      case "Low to High Price":
        return data.sort((a, b) => a.price - b.price);
      case "High to Low Price":
        return data.sort((a, b) => b.price - a.price);
      case "A-Z":
        return data.sort((a, b) => a.zoneName.localeCompare(b.zoneName));
      case "Z-A":
        return data.sort((a, b) => b.zoneName.localeCompare(a.zoneName));
      default:
        return data;
    }
  }
  return data;
}

const ReservationSection = ({data, reservationItems, setReservationItems}: Props) => {
  const dropdownData = ["Low to High Price", "High to Low Price", "A-Z", "Z-A"];
  const [dropdownValue, setDropdownValue] = useState<string>(dropdownData[0]);

  return (
    <div className="reservation-section-wrapper">
      <hr />
      <div className="sort-section">
        <h5>{data?.length} Options Available</h5>
        <div className="sort-combobox">
          <h5>Sort by</h5>
          <Combobox
            defaultValue={dropdownValue}
            style={{
              width: "180px",
              fontSize: "13px",
            }}
            value={dropdownValue}
            onChange={
              (value) =>{
                setDropdownValue(value);
                data = sortBy(value, data);
              } }
            data={dropdownData}
          />
        </div>
      </div>
      <hr />
      <div className="reservation-elements">
        {data != undefined?
          data.length !== 0? 
          data.map((ticket, index) => 
            <ReservationCard
              reservationItems={reservationItems}
              key={index}
              maximumQuantity={ticket.currentCapacity}
              setReservationItems={setReservationItems}
              days={ticket.days}
              price={ticket.price}
              zoneName={ticket.zoneName}
              daysDiscount={ticket.daysDiscount}
              earlyBirdDiscount={ticket.earlyBirdDiscount}
              raceIds={ticket.raceIds}
              zoneId={ticket.zoneId}
            />
          ):
          <h2>There is no tickets for provided filters</h2>
          :
          <h2>
            An unexpected error occurred
          </h2>
        }
      </div>
    </div>
  );
};

export default ReservationSection;
import React, { useEffect, useState } from "react";
import "./CartItems.scss";
import CartCard from "./CartCard";
import { ReservationItemModel } from "../../../Api/Reservation/ReservationApi.types";

type Props = {
  items: ReservationItemModel[],
  setItems: React.Dispatch<React.SetStateAction<ReservationItemModel[]>>;
  promoCodeDiscount: number;
  setPromoCodeDiscount: React.Dispatch<React.SetStateAction<number>>;
  totalPrice: number;
  setTotalPrice: React.Dispatch<React.SetStateAction<number>>;
};

const countCommas = (str: string): number => {
  return str.split(",").length;
};

const CartItems = ({items, setItems, promoCodeDiscount, setPromoCodeDiscount, totalPrice, setTotalPrice}: Props) => {
  const [earlyBirdDiscount, setEarlyBirdDiscount] = useState<number>(0);
  const [daysDiscount, setDaysDiscount] = useState<number>(0);

  useEffect(() => {
    calculatePrices();
    items.map((item, index) => {
      if(item.quantity === 0){
        setItems((prevItems) => prevItems.filter((_, i) => i !== index));
      }
    })
  }, [items, setItems])

  const decreaseQuantity = (index: number) => {
    setItems((prevItems) =>
      prevItems.map((item, i) =>
        i === index
          ? { ...item, quantity: Math.max(0, item.quantity - 1) }
          : item
      )
    );
  };

  const increaseQuantity = (index: number) => {
    setItems((prevItems) =>
      prevItems.map((item, i) =>
        i === index ? { ...item, quantity: item.quantity + 1 } : item
      )
    );
  };

  const calculatePrices = () => {
    setTotalPrice(0);
    setEarlyBirdDiscount(0);
    setDaysDiscount(0);
    items.map((item) => {
      setTotalPrice(prev => prev + (item.price * item.quantity));
      setDaysDiscount(prev => prev + (item.daysDiscount * item.quantity));
      setEarlyBirdDiscount(prev => prev + (item.earlyBirdDiscount * item.quantity));
    })
  }

  return (
    <div className="cart-items-wrapper">
      <h1>Cart</h1>
      <hr />
      <div className="items-section">
        { items.length === 0? 
        <h2>Add tickets to cart</h2>
          :items.map((item, index) => {
            return <CartCard
            key={index}
            earlyBirdDiscount={item.earlyBirdDiscount}
            days={item.days}
            maximumQuantity={item.maximumQuantity}
            itemNumber={index + 1}
            quantity={item.quantity}
            daysCount={countCommas(item.days)}
            pricePerDay={item.price}
            zoneName={item.zoneName}
            onDecreaseQuantity={() => decreaseQuantity(index)}
            onIncreaseQuantity={() => increaseQuantity(index)}
            onClick={() => {
              setItems((prevItems) => prevItems.filter((_, i) => i !== index));
            }}
          />
          }
        )}
      </div>
      <hr />
      <div className="early-bind-discount">
        <h4>Early bird discount</h4>
        <h3>{`-${earlyBirdDiscount.toFixed(2)} €`}</h3>
      </div>
      <div className="promo-code-discount">
        <h4>Promo code discount</h4>
        <h3>{`-${promoCodeDiscount.toFixed(2)} €`}</h3>
      </div>
      <div className="early-bind-discount">
        <h4>Number of days discount</h4>
        <h3>{`-${daysDiscount.toFixed(2)} €`}</h3>
      </div>
      <hr />
      <div className="total-price">
        <h1>Total</h1>
        <h1 id="price">{`${(
          totalPrice -
          earlyBirdDiscount -
          promoCodeDiscount -
          daysDiscount
        ).toFixed(2)} €`}</h1>
      </div>
      <hr />
    </div>
  );
};

export default CartItems;

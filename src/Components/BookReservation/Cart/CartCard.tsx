import React, { useState } from "react";
import "./CartCard.scss";
import { WeekendDays } from "../ReservationCard";
import SecondaryButton from "../../Shared/SecondaryButton";
import { CiCirclePlus, CiCircleMinus } from "react-icons/ci";

type Props = {
  zoneName: string;
  pricePerDay: number;
  earlyBirdDiscount: number;
  daysCount: number;
  days: string;
  itemNumber: number;
  quantity: number;
  maximumQuantity: number;
  onDecreaseQuantity?: React.MouseEventHandler<HTMLButtonElement> | undefined;
  onIncreaseQuantity?: React.MouseEventHandler<HTMLButtonElement> | undefined;
  onClick?: React.MouseEventHandler<HTMLInputElement> | undefined;
};

const CartCard = (props: Props) => {
  return (
    <div className="cart-card-wrapper">
      <div className="item-number">{props.itemNumber}</div>
      <div className="card-data">
        <div className="left-info">
          <div>
            <h3>{props.zoneName}</h3>
            <h5>{props.days}</h5>
          </div>
          <div className="quantity">
            <button
              type="button"
              onClick={props.onDecreaseQuantity}
              disabled={props.quantity === 0 ? true : false}
              style={{
                backgroundColor: props.quantity === 0 ? "grey" : "#E10600",
              }}
            >
              <CiCircleMinus size={27} color="white" />
            </button>
            <h5>{props.quantity}</h5>
            <button 
              type="button"
              onClick={props.onIncreaseQuantity}
              disabled={props.maximumQuantity === props.quantity}
              style={{
                backgroundColor: props.quantity === props.maximumQuantity ? "grey" : "#E10600",
              }}
              >
              <CiCirclePlus size={27} color="white" />
            </button>
          </div>
        </div>
        <div className="right-info">
          <s>
            {props.daysCount > 1 || props.earlyBirdDiscount > 0
              ? `${(props.pricePerDay * props.quantity)} €`
              : ""}
          </s>
          <div className="reservation-price">
            <h5>Price</h5>
            <h3>
              {props.daysCount > 1 || props.earlyBirdDiscount > 0
                ? `${(
                    props.pricePerDay *
                    props.quantity *
                    ((100 - 10 * (props.daysCount - 1)) / 100)
                  ).toFixed(1)} €`
                : `${(props.pricePerDay * props.quantity).toFixed(0)} €`}
            </h3>
          </div>
          <SecondaryButton
            button_value="Delete"
            width="60px"
            height="30px"
            margin="20px 0px 0px 0px"
            fontSize="14px"
            onClick={props.onClick}
          />
        </div>
      </div>
    </div>
  );
};

export default CartCard;

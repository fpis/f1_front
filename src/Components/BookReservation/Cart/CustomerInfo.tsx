import React, { useEffect, useState } from "react";
import "./CustomerInfo.scss";
import { CircularProgress, SelectChangeEvent, TextField } from "@mui/material";
import SelectCountry from "../../Shared/SelectCountry";
import LoadingButton from "@mui/lab/LoadingButton";
import { FaCheck } from "react-icons/fa6";
import { ImCross } from "react-icons/im";
import PopupModal from "../../Shared/PopupModal";
import SecondaryButton from "../../Shared/SecondaryButton";
import { useVerifyPromoCode } from "../../../Api/Reservation/ReservationApi";
import { MakeReservationModel, ReservationResponseModel } from "../../../Api/Reservation/ReservationApi.types";
import { useGetAllCountries } from "../../../Api/Country/CountryApi";
import { QueryObserverResult, RefetchOptions } from "@tanstack/react-query";
type Props = {
  handleSendReservation: () => void;
  reservationRefetch: (options?: RefetchOptions | undefined) => Promise<QueryObserverResult<ReservationResponseModel, Error>>;
  setReservationObject: React.Dispatch<React.SetStateAction<MakeReservationModel>>;
  response: ReservationResponseModel | undefined;
  setPromoCodeDiscount: React.Dispatch<React.SetStateAction<number>>;
  totalPrice: number;
  reservationData: ReservationResponseModel | undefined;
  reservationIsLoading: boolean;
  verifyingPromoCode: boolean;
  setVerifyingPromoCode: React.Dispatch<React.SetStateAction<boolean>>;
  reservationIsError: boolean;
};

const CustomerInfo = (props: Props) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [address1, setAddress1] = useState("");
  const [address2, setAddress2] = useState("");
  const [selectedCountry, setSelectedCountry] = useState("");
  const [postalCode, setPostalCode] = useState("");
  const [cityName, setCityName] = useState("");
  const [email, setEmail] = useState("");
  const [verifyEmail, setVerifyEmail] = useState("");
  const [promoCode, setPromoCode] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [verifyColor, setVerifyColor] = useState("grey");
  const [errorMessage, setErrorMessage] = useState("");

  const {data, error, isError, refetch} = useVerifyPromoCode(promoCode.toUpperCase());
  const {data: countryData, isError: countryIsError} = useGetAllCountries();

  const verifyFields = (): boolean => {
    if(firstName.length === 0){
      setErrorMessage("First name is empty")
      return false;
    }
    if(lastName.length === 0){
      setErrorMessage("Last name is empty")
      return false;
    }
    if(address1.length === 0){
      setErrorMessage("Address 1 is empty")
      return false;
    }
    if(selectedCountry.length === 0){
      setErrorMessage("Select country in list")
      return false;
    }
    if(postalCode.length === 0 || isNaN(Number(postalCode))){
      setErrorMessage("Postal code is empty or postal code is not number")
      return false;
    }
    if(cityName.length === 0){
      setErrorMessage("City name is empty")
      return false;
    }
    if(email.length === 0){
      setErrorMessage("Email is empty")
      return false;
    }
    if(verifyEmail.length === 0){
      setErrorMessage("Verify email is empty")
      return false;
    }
    setErrorMessage("");
    return true;
  };

  const setReservationFields = () => {
    const country = countryData?.find(country => country.name === selectedCountry)
    props.setReservationObject({
      address1: address1,
      cityName: cityName,
      countryId: countryData !== undefined && country !== undefined? country.countryId : 0,
      email: email,
      firstName: firstName,
      lastName: lastName,
      promoCode: promoCode.length === 0? undefined: promoCode,
      postalCode: Number(postalCode),
      reservationItems: []
    });
  }

  const clearTextBoxes = () => {
    setAddress1("");
    setAddress2("");
    setCityName("");
    setCompanyName("");
    setEmail("");
    setFirstName("");
    setLastName("");
    setPostalCode("");
    setPromoCode("");
    setVerifyEmail("");
    setSelectedCountry("");
  }

  const handleVerifyPromoCode = () => {
    refetch();
  }

  useEffect(() => {
    if(error){
      setErrorMessage(`Promo code isn't active or does not exist`);
      setVerifyColor("red");
    }else if(data){
      setErrorMessage("");
      setVerifyColor("green")
      props.setPromoCodeDiscount(0.05 * props.totalPrice)
    }
    props.setVerifyingPromoCode(false);
  }, [data, isError, error, refetch]);

  useEffect(() => {
    if(props.reservationData && props.reservationData?.promoCode && props.reservationData.token){
      setIsModalOpen(true);
    }
  }, [props.reservationData, props.reservationRefetch])

  return (
    <div className="customer-info-wrapper">
      <div className="title-info">
        <h1>Customer info</h1>
        <hr />
      </div>
      <div className="form">
        <h5>Enter first and last name</h5>
        <div className="first-last-name">
          <TextField
            required={true}
            value={firstName}
            error={firstName.length === 0}
            onChange={(e) => setFirstName(e.target.value)}
            label={isModalOpen ? "" : "First name"}
            variant="filled"
            className="text-box"
            style={{
              width: "200px",
              marginTop: "10px",
            }}
          />
          <TextField
            required={true}
            label={isModalOpen ? "" : "Last name"}
            value={lastName}
            error={lastName.length === 0}
            onChange={(e) => setLastName(e.target.value)}
            variant="filled"
            className="text-box"
            style={{
              width: "200px",
              marginTop: "10px",
            }}
          />
        </div>
        <h5>Enter company name(optional)</h5>
        <div>
          <TextField
            label={isModalOpen ? "" : "Company name"}
            variant="filled"
            value={companyName}
            onChange={(e) => setCompanyName(e.target.value)}
            className="text-box"
            style={{
              width: "300px",
              marginTop: "10px",
            }}
          />
        </div>
        <h5>Enter address</h5>
        <div className="address">
          <TextField
            required={true}
            label={isModalOpen ? "" : "Address 1"}
            error={address1.length === 0}
            value={address1}
            onChange={(e) => setAddress1(e.target.value)}
            variant="filled"
            className="text-box"
            style={{
              width: "200px",
              marginTop: "10px",
            }}
          />
          <TextField
            label={isModalOpen ? "" : "Address 2"}
            variant="filled"
            value={address2}
            onChange={(e) => setAddress2(e.target.value)}
            className="text-box"
            style={{
              width: "200px",
              marginTop: "10px",
            }}
          />
        </div>
        <div className="country">
          <SelectCountry
            data={countryData ? countryData : []}
            label={isModalOpen ? "" : "Country"}
            value={selectedCountry}
            onChange={(event) => {
              setSelectedCountry(event.target.value as string);
            }}
          />
          <TextField
            required={true}
            label={isModalOpen ? "" : "Postal code"}
            value={postalCode}
            error={postalCode.length === 0}
            onChange={(e) => setPostalCode(e.target.value)}
            variant="filled"
            className="text-box"
            style={{
              width: "150px",
              marginTop: "10px",
            }}
          />
          <TextField
            required={true}
            label={isModalOpen ? "" : "City"}
            value={cityName}
            error={cityName.length === 0}
            onChange={(e) => setCityName(e.target.value)}
            variant="filled"
            className="text-box"
            style={{
              width: "150px",
              marginTop: "10px",
            }}
          />
        </div>
        <h5>Enter email</h5>
        <div>
          <TextField
            required={true}
            label={isModalOpen ? "" : "Email"}
            value={email}
            error={email.length === 0}
            onChange={(e) => setEmail(e.target.value)}
            variant="filled"
            className="text-box"
            style={{
              width: "300px",
              marginTop: "10px",
            }}
          />
        </div>
        <div>
          <TextField
            required={true}
            label={isModalOpen ? "" : "Verify email"}
            value={verifyEmail}
            error={verifyEmail.length === 0 || email !== verifyEmail}
            onChange={(e) => setVerifyEmail(e.target.value)}
            variant="filled"
            className="text-box"
            style={{
              width: "300px",
              marginTop: "20px",
            }}
          />
        </div>
        <h5>Promo code (5% discount)</h5>
        <div className="promo-code">
          <TextField
            label={isModalOpen ? "" : "Promo code"}
            variant="filled"
            value={promoCode}
            onChange={(e) => setPromoCode(e.target.value)}
            className="text-box"
            style={{
              width: "300px",
              marginTop: "20px",
            }}
          />
          <LoadingButton
            loading={props.verifyingPromoCode}
            loadingPosition="start"
            style={{
              height: "40px",
              color: verifyColor,
              borderColor: "grey",
            }}
            onClick={() => {
              if(promoCode.length === 0){
                setErrorMessage("Enter promo code first");
                return;
              }
              props.setVerifyingPromoCode(true);
              handleVerifyPromoCode();
            }}
            startIcon={verifyColor === "red"? <ImCross/>: <FaCheck />}
            variant="outlined"
          >
            Verify
          </LoadingButton>
        </div>
      </div>
      <SecondaryButton
        height="50px"
        button_value={props.reservationIsLoading? "...": "Send reservation"}
        width={props.reservationIsLoading? "100px": "110%"}
        className="send-button"
        onClick={() => {
          if(verifyFields()){
            setReservationFields();
            props.handleSendReservation();
            props.setPromoCodeDiscount(0);
            clearTextBoxes();
          }
        }}
      />
      <h5>
        {errorMessage}
      </h5>
      <PopupModal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        width="500px"
        height="400px"
      >
        {props.reservationIsError ? 
          <div className="popup">
            <h2>Reservation failed</h2>
            <h4>An unexpected error occurred</h4>
          </div>
          : <div className="popup">
          <h2>Successful reservation</h2>
          <div className="popup-promo-code">
            <h4>Promo code</h4>
            <h3>{props.response?.promoCode}</h3>
          </div>
          <div className="popup-token">
            <h4>Token</h4>
            <h3>{props.response?.token}</h3>
          </div>
          <h5>
            This token is used to modify or delete a reservation. Keep it in
            safe place!
          </h5>
        </div>}

      </PopupModal>
    </div>
  );
};

export default CustomerInfo;

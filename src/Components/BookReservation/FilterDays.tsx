import React, { useEffect, useState } from "react";
import "./FilterDays.scss";
import FilterDayCard from "./FilterDayCard";
import { TicketFiltersModel } from "../../Api/Ticket/TicketApi.types";

type Props = {
  setDays: React.Dispatch<React.SetStateAction<number[]>>;
};

const FilterDays = (props: Props) => {
  const [isActive3Days, setIsActiveFirst] = useState<boolean>(true);
  const [isActive2Days, setIsActiveSecond] = useState<boolean>(false);
  const [isActive1Day, setIsActiveThird] = useState<boolean>(false);
  const dropdownDataOneDay = ["Friday", "Saturday", "Sunday"];
  const dropdownDataTwoDays = ["Friday, Saturday", "Friday, Sunday", "Saturday, Sunday"];
  const [twoDaysDropdownValue, setTwoDaysDropdownValue] = useState<string>(dropdownDataTwoDays[0]);
  const [oneDayDropdownValue, setOneDayDropdownValue] = useState<string>(dropdownDataOneDay[0]);

  useEffect(() => {
    switch(twoDaysDropdownValue){
      case "Friday, Saturday":
        props.setDays([1, 2])
        break;
      case "Friday, Sunday":
        props.setDays([1, 3])
        break;
      case "Saturday, Sunday":
        props.setDays([2, 3])
        break;
    }
  }, [twoDaysDropdownValue])

  useEffect(() => {
    switch (oneDayDropdownValue){
      case "Friday":
        props.setDays([1])
        break;
      case "Saturday":
        props.setDays([2])
        break;
      case "Sunday":
        props.setDays([3])
        break;
    }
  }, [oneDayDropdownValue])

  return (
    <div className="filter-days-wrapper">
      <div className="filter-cards">
        <FilterDayCard
          isActive={isActive3Days}
          minimalPrice={1000}
          haveDropdownList={false}
          numberOfDays={3}
          dropdownValue="Friday - Sunday"
          onClick={() => {
            setIsActiveFirst(true);
            setIsActiveSecond(false);
            setIsActiveThird(false);
            props.setDays([1, 2, 3]);
          }}
        />
        <FilterDayCard
          isActive={isActive2Days}
          minimalPrice={100}
          numberOfDays={2}
          haveDropdownList={true}
          dropdownValue={twoDaysDropdownValue}
          dropdownData={dropdownDataTwoDays}
          setDropdownValue={setTwoDaysDropdownValue}
          onClick={() => {
            setIsActiveFirst(false);
            setIsActiveSecond(true);
            setIsActiveThird(false);
            switch(twoDaysDropdownValue){
              case "Friday, Saturday":
                props.setDays([1, 2])
                break;
              case "Friday, Sunday":
                props.setDays([1, 3])
                break;
              case "Saturday, Sunday":
                props.setDays([2, 3])
                break;
            }
          }}
        />
        <FilterDayCard
          isActive={isActive1Day}
          minimalPrice={10}
          numberOfDays={1}
          haveDropdownList={true}
          dropdownData={dropdownDataOneDay}
          dropdownValue={oneDayDropdownValue}
          setDropdownValue={setOneDayDropdownValue}
          onClick={() => {
            setIsActiveFirst(false);
            setIsActiveSecond(false);
            setIsActiveThird(true);
            switch (oneDayDropdownValue){
              case "Friday":
                props.setDays([1])
                break;
              case "Saturday":
                props.setDays([2])
                break;
              case "Sunday":
                props.setDays([3])
                break;
            }
          }}
        />
      </div>
      <div className="vl"></div>
    </div>
  );
};

export default FilterDays;

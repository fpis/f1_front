import React, { useEffect, useState } from "react";
import "./Filters.scss";
import { Slider } from "@mui/material";
import Switch from "react-switch";
import { TbDisabled } from "react-icons/tb";
import { CgScreen } from "react-icons/cg";
import PrimaryButton from "../Shared/PrimaryButton";
import SecondaryButton from "../Shared/SecondaryButton";

type Props = {
  sliderValue: number[];
  setSliderValue: React.Dispatch<React.SetStateAction<number[]>>;
  minMaxValue: number[];
  setMinMacValue: React.Dispatch<React.SetStateAction<number[]>>;
  checkedDisabled: boolean;
  setCheckedDisabled: React.Dispatch<React.SetStateAction<boolean>>;
  checkedLargeTv: boolean;
  setCheckedLargeTv: React.Dispatch<React.SetStateAction<boolean>>;
  setFilters: () => void
};

function valuetext(value: number) {
  return `${value}°C`;
}

const Filters = (props: Props) => {

  const handleChangeSlider = (event: Event, newValue: number | number[]) => {
    const [newValueLower, newValueHigher] = newValue as number[];
    if (newValueLower >= newValueHigher) {
      return;
    } else {
      props.setSliderValue(newValue as number[]);
    }
  };

  return (
    <div className="filters-wrapper">
      <div className="filter-title">
        <SecondaryButton 
        button_value="Apply filters"
        margin="10px"
        width="100px"
        onClick={() => {
          props.setFilters();
        }}
        />
        <div className="vertical-line"></div>
      </div>
      <div className="price-range">
        <div className="price-range-main">
          <h5>Price</h5>
          <div className="slider-wrapper">
            <h5>€ {props.sliderValue[0]}</h5>
            <Slider
              max={props.minMaxValue[1]}
              min={props.minMaxValue[0]}
              getAriaLabel={() => "Temperature range"}
              value={props.sliderValue}
              onChange={handleChangeSlider}
              valueLabelDisplay="off"
              getAriaValueText={valuetext}
              sx={{
                width: "300px",
                marginLeft: "15px",
                marginRight: "15px",
                color: "#E10600",
              }}
            />
            <h5>€ {props.sliderValue[1]}</h5>
          </div>
        </div>
        <div className="vertical-line"></div>
      </div>
      <div className="disabled-access">
        <div className="disabled-access-main">
          <h5>Disabled access</h5>
          <Switch
            checked={props.checkedDisabled}
            onChange={(checked) => props.setCheckedDisabled(checked)}
            uncheckedIcon={false}
            checkedIcon={false}
            onColor="#E10600"
            checkedHandleIcon={
              <TbDisabled
                color="black"
                size={22}
                style={{
                  marginTop: "2px",
                  marginLeft: "2px",
                }}
              />
            }
            uncheckedHandleIcon={
              <TbDisabled
                color="black"
                size={22}
                style={{
                  marginTop: "2px",
                  marginLeft: "2px",
                }}
              />
            }
          />
        </div>
        <div className="vertical-line"></div>
      </div>
      <div className="giant-screen">
        <div className="giant-screen-main">
          <h5>Giant screen</h5>
          <Switch
            checked={props.checkedLargeTv}
            onChange={(checked) => props.setCheckedLargeTv(checked)}
            uncheckedIcon={false}
            checkedIcon={false}
            onColor="#E10600"
            checkedHandleIcon={
              <CgScreen
                color="black"
                size={22}
                style={{
                  marginTop: "3px",
                  marginLeft: "2px",
                }}
              />
            }
            uncheckedHandleIcon={
              <CgScreen
                color="black"
                size={22}
                style={{
                  marginTop: "3px",
                  marginLeft: "2px",
                }}
              />
            }
          />
        </div>
        <div className="vertical-line"></div>
      </div>
      <div className="reset-filters">
        <PrimaryButton
          button_value="Reset"
          onClick={() => {
            props.setCheckedDisabled(false);
            props.setCheckedLargeTv(true);
            props.setSliderValue([150, 5500]);
          }}
        ></PrimaryButton>
      </div>
    </div>
  );
};

export default Filters;

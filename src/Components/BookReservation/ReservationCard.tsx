import React from "react";
import "./ReservationCard.scss";
import SecondaryButton from "../Shared/SecondaryButton";
import { ReservationItemModel } from "../../Api/Reservation/ReservationApi.types";

export type WeekendDays = "Friday" | "Saturday" | "Sunday";

type Props = {
  zoneId: number;
  raceIds: number[];
  zoneName: string;
  price: number;
  days: string;
  daysDiscount: number;
  earlyBirdDiscount: number;
  maximumQuantity: number;
  reservationItems: ReservationItemModel[];
  setReservationItems: React.Dispatch<React.SetStateAction<ReservationItemModel[]>>;
};

const ReservationCard = (props: Props) => {
  const totalDiscount = props.daysDiscount + props.earlyBirdDiscount;
  return (
    <div className="reservation-card">
      <div className="left-info">
        <h3>{props.zoneName}</h3>
        <h5>{props.days}</h5>
      </div>
      <div className="right-info">
        <s>
          {totalDiscount !== 0 ? `${props.price} €` : ""}
        </s>
        <div className="reservation-price">
          <h5>Price</h5>
          <h3>
            {totalDiscount !== 0
              ? `${props.price - totalDiscount} €`
              : `${props.price} €`}
          </h3>
        </div>
        <SecondaryButton
          disabled={() => {
            let itemIndex = props.reservationItems.findIndex(item => item.zoneName === props.zoneName);
            if(itemIndex === -1)
              return false;
            return props.reservationItems[itemIndex].quantity >= props.maximumQuantity
          }}
          button_value="ADD TO CART"
          onClick={() => {
            let itemIndex = props.reservationItems.findIndex(item => item.zoneName === props.zoneName && item.days === props.days);
            if(itemIndex !== -1){
              props.setReservationItems(prev =>{
                  let updatedItems = [...prev];
                  updatedItems[itemIndex].quantity += 1;
                  return updatedItems;
                }  
              )
            }
            else{
              props.setReservationItems(prev => 
                [...prev,
                  {
                    days: props.days,
                    daysDiscount: props.daysDiscount,
                    earlyBirdDiscount: props.earlyBirdDiscount,
                    maximumQuantity: props.maximumQuantity,
                    price: props.price,
                    quantity: 1,
                    zoneName: props.zoneName,
                    raceIds: props.raceIds,
                    zoneId: props.zoneId
                  }
                ]
              )
            }
          }}
          width="90px"
          height="40px"
          className="button-add-cart"
          fontSize="13px"
          margin="30px 0px 0px 0px"
        />
      </div>
    </div>
  );
};

export default ReservationCard;

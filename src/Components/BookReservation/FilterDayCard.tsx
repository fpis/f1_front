import React from "react";
import "./FilterDayCard.scss";
import { Combobox } from "react-widgets/cjs";

type Props = {
  numberOfDays: number;
  minimalPrice: number;
  isActive: boolean;
  haveDropdownList: boolean;
  dropdownData?: string[];
  dropdownValue?: string; 
  setDropdownValue?: React.Dispatch<React.SetStateAction<string>>;
  onClick?: React.MouseEventHandler<HTMLInputElement> | undefined;
};

const FilterDayCard = (props: Props) => {
  return (
    <div className="card-wrapper">
      <hr className={props.isActive ? " active-hr" : " inactive-hr"} />
      <div
        className={
          "filter-day-card-wrapper" +
          (props.isActive ? " active-card" : " inactive-card")
        }
        onClick={props.onClick}
      >
        <div className="vertical-line"></div>
        <div className="card-info-wrapper">
          {props.haveDropdownList && props.isActive && props.setDropdownValue !== undefined? 
            <Combobox
            defaultValue={props.dropdownValue}
            style={{
              width: "180px",
              fontSize: "13px",
            }}
            onChange={
              (value) => {
                props.setDropdownValue!(value);
              } 
            }
            value={props.dropdownValue}
            data={props.dropdownData}
          />
          : <h3>{props.dropdownValue}</h3>
          }  
          <h1>{props.numberOfDays}</h1>
          <h5>{props.numberOfDays == 1 ? "day" : "days"}</h5>
        </div>
      </div>
      <hr className={props.isActive ? " active-hr" : " inactive-hr"} />
    </div>
  );
};

export default FilterDayCard;

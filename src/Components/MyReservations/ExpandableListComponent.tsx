import React, { useEffect, useState } from "react";
import "./ExpandableListComponent.scss";
import { TiPlus, TiMinus } from "react-icons/ti";
import ReservationCard from "./ReservationCard";
import SecondaryButton from "../Shared/SecondaryButton";
import { GetReservationModel, PatchReservationItemsModel } from "../../Api/Reservation/ReservationApi.types";
import { useDeleteReservation } from "../../Api/Reservation/ReservationApi";
import { QueryObserverResult, RefetchOptions } from "@tanstack/react-query";


type Props = {
  data: Array<GetReservationModel>;
  refetch: (options?: RefetchOptions | undefined) => Promise<QueryObserverResult<GetReservationModel, Error>>
  handleOpenModal: () => void;
  modalMessage: string;
  setModalMessage: React.Dispatch<React.SetStateAction<string>>;
  patchItems: PatchReservationItemsModel[];
  setPatchItems: React.Dispatch<React.SetStateAction<PatchReservationItemsModel[]>>;
  setTotalPrice: React.Dispatch<React.SetStateAction<number>>;
  setPromoCodeDiscount: React.Dispatch<React.SetStateAction<number>>;
  calculatePrices: () => void;
};

const ExpandableListComponent = (props: Props) => {
  const [selected, setSelected] = useState<number | null>(null);
  const [reservationId, setReservationId] = useState<number>(0);
  const {data: deleteData, refetch: deleteRefetch, isError: deleteIsError, isLoading: deleteIsLoading, isRefetching: deleteIsRefetching} = useDeleteReservation(reservationId);
  const [handleCalculate, setHandleCalculate] = useState<boolean | undefined>(undefined);

  useEffect(()=>{
    if(reservationId !== 0){
      deleteRefetch();
      props.setTotalPrice(0);
      props.setPromoCodeDiscount(0);
    }
  }, [reservationId, setReservationId]);

  useEffect(() => {
    if(props.data[0] !== undefined){
      props.setPatchItems(props.data[0].reservationItems.map((item, i) => ({
        quantity: item.quantity,
        serialNumber: item.serialNumber
      })));
    }
  }, [props.data, props.refetch]);

  useEffect(() => {
    if(deleteData){
      if(deleteIsError){
        props.setModalMessage("Error while deleting reservation. Try again.");
      }
      else{
        props.setModalMessage("Reservation deleted successfully");
      }
    }
  }, [deleteData, deleteRefetch]);

  useEffect(() => {
    if(props.modalMessage !== ""){
      props.handleOpenModal();
    }
  }, [props.modalMessage, props.setModalMessage]);

  useEffect(() => {
    if(handleCalculate !== undefined){
      props.calculatePrices();
    }
  }, [handleCalculate, setHandleCalculate]);

  const toggle = (i: number) => {
    if (selected === i) {
      return setSelected(null);
    }
    return setSelected(i);
  };

  const decreaseQuantity = (serialNumber: number) => {
    props.setPatchItems((prevItems) =>
      prevItems.map((item, i) =>
        item.serialNumber === serialNumber
          ? { ...item, quantity: Math.max(0, item.quantity - 1) }
          : item
      )
    );
    setHandleCalculate(prev => prev !== undefined ? !prev : true );
  };

  const increaseQuantity = (serialNumber: number) => {
    props.setPatchItems((prevItems) =>
      prevItems.map((item, i) =>
        item.serialNumber === serialNumber ? { ...item, quantity: item.quantity + 1 } : item
      )
    );
    setHandleCalculate(prev => prev !== undefined ? !prev : true );
  };

  return (
    props.data.length === 0 ?
    <div className="no-items-found">
      <h1>No reservation found</h1>
    </div>
    :<div className="expandable-list-wrapper">
      {props.data.map((item, i) => (
        <div className="item">
          <div className="title-item" onClick={() => toggle(i)}>
            <div className="title-info">
              <h1>{`${item.customerFullName}'s reservation`}</h1>
              <h2>{(new Date(item.reservationDate)).toDateString()}</h2>
            </div>
            <div className="item-end">
              <SecondaryButton 
                button_value="Delete"
                height="35px"
                width="90px"
                onClick={() => {
                  setReservationId(item.reservationId);
                }}
              />
              {selected === i ? (
                <TiMinus
                  size={30}
                  style={{
                    marginRight: "50px",
                  }}
                />
              ) : (
                <TiPlus
                  size={30}
                  style={{
                    marginRight: "50px",
                  }}
                />
              )}
            </div>
          </div>
          <div className={selected === i ? "content show" : "content"}>
            {item.reservationItems.map((item2, i2) => (
              <ReservationCard
                maximumQuantity={item2.maximumQuantity}
                serialNumber={item2.serialNumber}
                onDecreaseQuantity={() => decreaseQuantity(item2.serialNumber)}
                onIncreaseQuantity={() => increaseQuantity(item2.serialNumber)}
                price={item2.price}
                itemNumber={i2 + 1}
                quantity={props.patchItems.find(it => it.serialNumber === item2.serialNumber)?.quantity!}
                zoneName={item2.zoneName}
                days={item2.days}
              />
            ))}
          </div>
        </div>
      ))}
    </div>
  );
};

export default ExpandableListComponent;

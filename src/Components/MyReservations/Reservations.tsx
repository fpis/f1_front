import React, { useEffect, useState } from "react";
import "./Reservations.scss";
import ExpandableListComponent from "./ExpandableListComponent";
import { TextField } from "@mui/material";
import SecondaryButton from "../Shared/SecondaryButton";
import PrimaryButton from "../Shared/PrimaryButton";
import { useGetReservation, usePatchReservation } from "../../Api/Reservation/ReservationApi";
import { GetReservationModel, PatchReservationItemsModel, PatchReservationModel } from "../../Api/Reservation/ReservationApi.types";
import PopupModal from "../Shared/PopupModal";

type Props = {};

const Reservations = (props: Props) => {
  const [email, setEmail] = useState("");
  const [token, setToken] = useState("");
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false); 
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState<boolean>(false); 
  const {data, refetch, isError, isLoading, isRefetching} = useGetReservation(token, email);
  const [reservation, setReservation] = useState<GetReservationModel[]>([]);
  const [modalMessage, setModalMessage] = useState<string>("");
  const [updateModalMessage, setUpdateModalMessage] = useState<string>("");
  const [promoCodeDiscount, setPromoCodeDiscount] = useState<number>(0);
  const [daysDiscountPrecentage, setDaysDiscountPrecentage] = useState<number>(0);
  const [earlyBirdDiscountPrecentage, setEarlyBirdDiscountPercentage] = useState<number>(0);
  const [patchReservation, setPatchReservation] = useState<PatchReservationModel>({
    reservationId: 0,
    reservationItems: [],
    token: ""
  });
  const {data: patchData, refetch: patchRefetch, isError: patchIsError, isLoading: patchIsLoading, isRefetching: patchIsRefetching} = usePatchReservation(patchReservation);
  const [patchItems, setPatchItems] = useState<PatchReservationItemsModel[]>([]);
  const [totalPrice, setTotalPrice] = useState<number>(0);

  useEffect(() => {
    if(data !== undefined){
      setReservation([data]);
    }
  }, [data]);

  useEffect(() => {
    if(reservation.length !== 0){
      setEarlyBirdDiscountPercentage(reservation[0].earlyBirdDiscount / reservation[0].totalPrice);
      setDaysDiscountPrecentage(reservation[0].daysDiscount / reservation[0].totalPrice);
      setTotalPrice(reservation[0].totalPrice);
      setPromoCodeDiscount(reservation[0].promoCodeDiscount);
    }
  }, [reservation, setReservation]);

  useEffect(() => {
    if(token !== ""){
      setToken("");
      setEmail("");
      setTotalPrice(0);
      setPromoCodeDiscount(0);
      patchRefetch();
    }
  }, [patchReservation, setPatchReservation]);

  useEffect(() => {
    if(patchData){
      if(patchIsError){
        setUpdateModalMessage("Error while updating reservation. Try again.");
      }
      else{
        setUpdateModalMessage("Reservation updated successfully");
        refetch();
      }
      setIsUpdateModalOpen(true);
    }
  }, [patchData, patchRefetch]);

  useEffect(() => {
    if(promoCodeDiscount !== 0){
      setPromoCodeDiscount(0.05 * totalPrice);
    }
  }, [totalPrice])

  const prepareForSend = () => {
    if(data !== undefined){
      data.reservationItems.map((item, i) => {
        patchItems.map((item2, i2) => {
          if(item2.serialNumber === item.serialNumber && item.quantity === item2.quantity){
            const newArray = patchItems.filter(it => it.serialNumber !== item2.serialNumber);
            setPatchItems(newArray);
          }
        });
      });
    }
  }

  const getItemPrice = (serialNumber: number) => {
    let returnValue = 0;
    data?.reservationItems.forEach(element => {
      if(element.serialNumber === serialNumber)
        returnValue = element.fullPrice;
    });
    return returnValue;
  }

  return (
    <div className="reservations-wrapper">
      <div className="title">
        <h1>My reservations</h1>
      </div>
      <hr />
      <h2>Enter email and token</h2>
      <div className="text-boxes">
          <div>
            <TextField
              label={isModalOpen? "" : "Email"}
              variant="filled"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="text-box"
              style={{
                width: "300px",
                marginTop: "20px",
              }}
            />
          </div>
          <div>
            <TextField
              label={isModalOpen? "" : "Token"}
              variant="filled"
              value={token}
              onChange={(e) => setToken(e.target.value)}
              className="text-box"
              style={{
                width: "300px",
                marginTop: "20px",
              }}
            />
          </div>
          <PrimaryButton 
            button_value="Search"
            onClick={() => {
              refetch();
            }} 
          />
        </div>
        <hr />

      <ExpandableListComponent
        patchItems={patchItems}
        setPatchItems={setPatchItems}
        setPromoCodeDiscount={setPromoCodeDiscount}
        setTotalPrice={setTotalPrice}
        calculatePrices={() => {
          let total = 0;
          patchItems.forEach(e => {
            total += e.quantity * getItemPrice(e.serialNumber);
          });
          setTotalPrice(total);
        }}
        data={!isError ? reservation : []}
        handleOpenModal={() => {
          setIsModalOpen(true);
          refetch();
        }}
        modalMessage={modalMessage}
        refetch={refetch}
        setModalMessage={setModalMessage}
      />
      <div>
        <hr />
        <div className="promo-code-discount">
          <h4>Promo code discount</h4>
          <h3>{`-${promoCodeDiscount.toFixed(2)} €`}</h3>
        </div>
        <hr />
        <div className="total-price">
          <h1>New price</h1>
          <h1 id="price">{`${(
            totalPrice - promoCodeDiscount - daysDiscountPrecentage * totalPrice - earlyBirdDiscountPrecentage * totalPrice
          ).toFixed(2)} €`}</h1>
        </div>
        <hr />
      </div>
      <div className="save-reservation">
        <SecondaryButton
          onClick={() => {
            if(data !== undefined){
              prepareForSend();
              setPatchReservation(
                {
                  reservationId: data.reservationId,
                  token: token,
                  reservationItems: patchItems
                }
              );
            }
          }}
          button_value="Save changes"
          height="40px"
          width="120%"
        />
      </div>
      <PopupModal
        isOpen={isModalOpen}
        onRequestClose={() => {setIsModalOpen(false);}}
        width="400px"
        height="400px"
      >
        <div className="popup">
          <div className="title">
            <h1>
              Delete message
            </h1>
          </div>
          <h2>
            {modalMessage}
          </h2>
        </div>
      </PopupModal>
      <PopupModal
        isOpen={isUpdateModalOpen}
        onRequestClose={() => {setIsUpdateModalOpen(false);}}
        width="400px"
        height="400px"
      >
        <div className="popup">
          <div className="title">
            <h1>
              Update message
            </h1>
          </div>
          <h2>
            {updateModalMessage}
          </h2>
        </div>
      </PopupModal>
    </div>
  );
};

export default Reservations;

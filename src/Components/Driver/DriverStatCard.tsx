import React from "react";
import "./DriverStatCard.scss";
import Reveal from "../Shared/Reveal";

type Props = {
  text: string;
  value: string;
  color: string;
};

const DriverStatCard = (props: Props) => {
  return (
    <Reveal>
      <div className="driver-stat-card-wrapper">
        <div className="stat-card-text">
          <h4>{props.text}</h4>
        </div>
        <div className="stat-card-value" style={{backgroundColor: `${props.color}`}}>
          <h3>{props.value}</h3>
        </div>
      </div>
    </Reveal>
  );
};

export default DriverStatCard;

import React from "react";
import "./DriverCover.scss";
import Reveal from "../Shared/Reveal";
import { FlagIcon, FlagIconCode } from "react-flag-kit";

type Props = {
  driverName: string;
  driverSurname: string;
  driverPicture: string;
  driverNo: number;
  driverCountry: string;
  colorHex: string;
  countryShortName: FlagIconCode;
};

const DriverCover = (props: Props) => {
  return (
    <div className="driver-cover-wrapper">
      <div className="cover-left">
        <div className="name">
          <div className="vertical-line" style={{borderLeft: `6px solid ${props.colorHex}`, height: "100px"}}></div>
          <div className="driver-name">
            <Reveal>
              <h5>{props.driverName}</h5>
            </Reveal>
            <Reveal>
              <h3>{props.driverSurname.toUpperCase()}</h3>
            </Reveal>
          </div>
        </div>
        <div className="driver-country">
          <FlagIcon code={props.countryShortName} width={50} height={40} />
        </div>
      </div>
      <div className="cover-right">
        <h1>
          <i>{props.driverNo}</i>
        </h1>
        <Reveal>
          <img src={props.driverPicture} alt="driver" />
        </Reveal>
      </div>
    </div>
  );
};

export default DriverCover;

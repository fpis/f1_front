import React from "react";
import "./DriverInfo.scss";
import DriverStatCard from "./DriverStatCard";

type Props = {
  teamName: string;
  poduims: number;
  points: number;
  worldChampionchip: number;
  dateOfBirth: Date;
  placeOfBirth: string;
  pictureHelmetUrl: string;
  biography: string;
  colorHex: string;
};

const DriverInfo = (props: Props) => {
  const text = props.biography;
  const newText = text.split("\n").map((str) => <p>{str}</p>);

  return (
    <div className="driver-info-wrapper">
      <div className="vertical-line" style={{borderLeft: `15px solid ${props.colorHex}`, height: "160vh"}}></div>
      <div className="main-info">
        <div className="top-main-info">
          <img src={props.pictureHelmetUrl} alt="" />
          <h1>{props.teamName}</h1>
        </div>
        <hr />
        <div className="info">
          <div className="birth-info">
            <div>
              <h3>Place of birth</h3>
              <h5>{props.placeOfBirth}</h5>
            </div>
            <div>
              <h3>Date of birth</h3>
              <h5>{(props.dateOfBirth.toString().slice(0, props.dateOfBirth.toString().indexOf("T")))}</h5>
            </div>
          </div>
          <div className="stat-info">
            <DriverStatCard
              text="World championchips"
              value={props.worldChampionchip.toString()}
              color={props.colorHex}
            />
            <DriverStatCard text="Points" value={props.points.toString()} color={props.colorHex}/>
            <DriverStatCard text="Podiums" value={props.poduims.toString()} color={props.colorHex}/>
          </div>
        </div>
        <hr />
        <div className="biography">
          <h1>Biography</h1>
          <p>{newText}</p>
        </div>
      </div>
    </div>
  );
};

export default DriverInfo;

import { FlagIconCode } from "react-flag-kit";

export interface CountryModel{
    countryId: number;
    name: string;
    shortName: FlagIconCode;
}
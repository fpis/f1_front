import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { F1_API_BASE_URL } from "../ApiConfig";
import { CountryModel } from "./CountryApi.types";

const getAllCountries = async (): Promise<CountryModel[]> => {
    const response = await axios.get<CountryModel[]>(
        `${F1_API_BASE_URL}country/get-all`
    );
    return response.data;
};

export const useGetAllCountries = () => {
    const query = useQuery<CountryModel[], Error>({
        queryKey: ["get-countries-key"],
        queryFn: async () => await getAllCountries()
    });
    return query;
};
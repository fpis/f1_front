import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { F1_API_BASE_URL } from "../ApiConfig";
import { GrandPrixModel } from "./GrandPrixApi.types";

const getGrandPrixByName = async (
    name: string
): Promise<GrandPrixModel> => {
    const response = await axios.get<GrandPrixModel>(
        `${F1_API_BASE_URL}grand-prix/get-by-name`,
        {
            params: {
                name: name
            },
        }
    );
    return response.data;
};

export const useGetGrandPrixByName = (name: string) => {
    const query = useQuery<GrandPrixModel, Error>({
        queryKey: ["get-grand-prix-key"],
        queryFn: async () => await getGrandPrixByName(name)
    });
    return query;
};

export interface GrandPrixModel{
    grandPrixId: number;
    city: string;
    location: string;
    startDate: Date;
    endDate: Date;
    additionalInformation: string;
    firstGrandPrix: number;
    numberOfLaps: number;
    circuitLength: number;
    raceDistance: number;
    lapRecord: string;
    lapRecordDriver: string;
}
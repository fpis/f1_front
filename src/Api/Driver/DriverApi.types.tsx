import { FlagIconCode } from "react-flag-kit";

export interface DriverFullModel {
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  placeOfBirth: string;
  totalPoints: number;
  number: number;
  teamName: string;
  thisYearPoints: number;
  pictureUrl: string;
  countryShortName: FlagIconCode;
  colorHex: string;
  podiums: number;
  worldChampionships: number;
  pictureHelmetUrl: string;
  country: string;
  biography: string;
}
export interface DriverShortModel{
  driverId: number;
  firstName: string;
  lastName: string;
  teamName: string;
  thisYearPoints: number;
  pictureUrl: string;
  countryShortName: FlagIconCode;
  number: number;
  colorHex: string;
}

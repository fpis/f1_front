import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { F1_API_BASE_URL } from "../ApiConfig";
import { DriverFullModel, DriverShortModel } from "./DriverApi.types";

const getDriversByGrandPrix = async (
    page: number,
    size: number,
    grandPrixId: number
): Promise<DriverShortModel[]> => {
    const response = await axios.get<DriverShortModel[]>(
        `${F1_API_BASE_URL}driver/get-drivers`,
        {
            params: {
                page: page,
                size: size,
                grandPrixId: grandPrixId
            },
        }
    );
    return response.data;
};

export const useGetDriversByGrandPrix = (page: number, size: number, grandPrixId: number) => {
    const query = useQuery<DriverShortModel[], Error>({
        queryKey: ["get-drivers-key"],
        queryFn: async () => await getDriversByGrandPrix(page, size, grandPrixId)
    });
    return query;
};

const getDriverById = async (
    driverId: number
): Promise<DriverFullModel> => {
    const response = await axios.get<DriverFullModel>(
        `${F1_API_BASE_URL}driver/get-driver`,
        {
            params: {
                driverId: driverId
            },
        }
    );
    return response.data;
};

export const useGetDriverById = (driverId: number) => {
    const query = useQuery<DriverFullModel, Error>({
        queryKey: ["get-driver-by-id-key"],
        queryFn: async () => await getDriverById(driverId)
    });
    return query;
};

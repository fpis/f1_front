import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { F1_API_BASE_URL } from "../ApiConfig";
import { GetReservationModel, MakeReservationModel, PatchReservationModel, ReservationResponseModel } from "./ReservationApi.types";

const verifyPromoCode = async (promoCode: string): Promise<string> => {
    const response = await axios.get<string>(
        `${F1_API_BASE_URL}reservation/verify-promo-code`,
        {
            params: {
                promoCode: promoCode
            }
        }
    );
    return response.data;
};

export const useVerifyPromoCode = (promoCode: string) => {
    const query = useQuery<string, Error>({
        queryKey: ["get-verify-promo-code-key"],
        queryFn: async () => await verifyPromoCode(promoCode),
        enabled: false,
        retry: 0
    });
    return query;
};

const makeReservation = async (
    reservationObject: MakeReservationModel
): Promise<ReservationResponseModel> => {
    const response = await axios.post<ReservationResponseModel>(
        `${F1_API_BASE_URL}reservation/make-reservation`,
        reservationObject
    );
    return response.data;
};

export const useMakeReservation = (reservationObject: MakeReservationModel) => {
    const query = useQuery<ReservationResponseModel, Error>({
        queryKey: ["post-make-reservation-key"],
        queryFn: async () => await makeReservation(reservationObject),
        enabled: false
    });
    return query;
};

const getReservation = async (
    token: string,
    email: string
): Promise<GetReservationModel> => {
    const response = await axios.get<GetReservationModel>(
        `${F1_API_BASE_URL}reservation/get-reservation`,
        {
            params: {
                token: token,
                email: email
            }
        }
    );
    return response.data;
};

export const useGetReservation = (token: string, email: string) => {
    const query = useQuery<GetReservationModel, Error>({
        queryKey: ["get-reservation-key"],
        queryFn: async () => await getReservation(token, email),
        enabled: false,
        retry: 0
    });
    return query;
};

const deleteReservation = async (
    reservationId: number
): Promise<string> => {
    const response = await axios.delete<string>(
        `${F1_API_BASE_URL}reservation/delete-reservation`,
        {
            params: {
                reservationId: reservationId
            }
        }
    );
    return response.data;
};

export const useDeleteReservation = (reservationId: number) => {
    const query = useQuery<string, Error>({
        queryKey: ["delete-reservation-key"],
        queryFn: async () => await deleteReservation(reservationId),
        enabled: false
    });
    return query;
};

const patchReservation = async (
    data: PatchReservationModel
): Promise<string> => {
    const response = await axios.patch<string>(
        `${F1_API_BASE_URL}reservation/patch-reservation`,
        data
    );
    return response.data;
};

export const usePatchReservation = (data: PatchReservationModel) => {
    const query = useQuery<string, Error>({
        queryKey: ["patch-reservation-key"],
        queryFn: async () => await patchReservation(data),
        enabled: false,
    });
    return query;
};

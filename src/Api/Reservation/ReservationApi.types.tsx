
export interface ReservationItemModel {
  zoneName: string;
  price: number;
  maximumQuantity: number;
  days: string;
  earlyBirdDiscount: number;
  daysDiscount: number;
  quantity: number;
  zoneId: number;
  raceIds: number[];
}

export interface MakeReservationItemModel{
  raceIds: number[];
  zoneId: number;
  quantity: number;
}

export interface MakeReservationModel{
  firstName: string;
  lastName: string;
  companyName?: string;
  address1: string;
  address2?: string;
  countryId: number;
  postalCode: number;
  cityName: string;
  email: string;
  promoCode?: string;
  reservationItems: MakeReservationItemModel[];
}

export interface ReservationResponseModel{
  promoCode: string;
  token: string;
}

export interface GetReservationItemsModel{
  quantity: number;
  maximumQuantity: number;
  days: string;
  price: number;
  fullPrice: number;
  serialNumber: number;
  zoneName: string;
}

export interface GetReservationModel{
  reservationId: number;
  reservationDate: Date;
  daysDiscount: number;
  promoCodeDiscount: number;
  earlyBirdDiscount: number;
  totalPrice: number;
  customerFullName: string;
  reservationItems: GetReservationItemsModel[];
}

export interface PatchReservationItemsModel{
  serialNumber: number;
  quantity: number;
}

export interface PatchReservationModel{
  reservationId: number;
  token: string;
  reservationItems: PatchReservationItemsModel[];
}

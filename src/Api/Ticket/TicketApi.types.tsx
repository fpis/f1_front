export interface TicketModel{
    price: number;
    earlyBirdDiscount: number;
    daysDiscount: number;
    currentCapacity: number;
    zoneName: string;
    days: string;
    raceDate: Date;
    raceIds: number[];
    zoneId: number;
}

export interface TicketFiltersModel{
    hasLargeTv: boolean;
    suitableForDisabled: boolean;
    lowPrice: number;
    highPrice: number;
    days: number[];
}
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { F1_API_BASE_URL } from "../ApiConfig";
import { TicketModel, TicketFiltersModel } from "./TicketApi.types";

const getTicketFiltered = async (
    filters: TicketFiltersModel
): Promise<TicketModel[]> => {
    const response = await axios.post<TicketModel[]>(
        `${F1_API_BASE_URL}ticket/get-tickets`,
        {
            hasLargeTv: filters.hasLargeTv,
            suitableForDisabled: filters.suitableForDisabled,
            lowPrice: filters.lowPrice,
            highPrice: filters.highPrice,
            days: filters.days
        }
    );
    return response.data;
};

export const useGetTicketsFiltered = (filters: TicketFiltersModel) => {
    const query = useQuery<TicketModel[], Error>({
        queryKey: ["get-tickets-key"],
        queryFn: async () => await getTicketFiltered(filters)
    });
    return query;
};

import React from "react";
import DriverCover from "../Components/Driver/DriverCover";
import DriverInfo from "../Components/Driver/DriverInfo";
import { useGetDriverById } from "../Api/Driver/DriverApi";
import {useParams} from "react-router-dom";

type Props = {
};

const Driver = (props: Props) => {
  const { driverId } = useParams();
  const { data, isError, refetch, isLoading, isRefetching } = useGetDriverById(driverId !== undefined ? parseInt(driverId, 10): 0);
  return (
    <div>
      { data === undefined ? 
      <div></div>
      :<DriverCover
          countryShortName={data.countryShortName}
          driverName={data.firstName}
          driverCountry={data.country}
          driverNo={data.number}
          driverPicture={data.pictureUrl}
          driverSurname={data.lastName}
          colorHex={data.colorHex}
        />
      }
      {
        data === undefined ?
        <div></div>
        :<DriverInfo
        biography={data.biography}
        dateOfBirth={data.dateOfBirth}
        placeOfBirth={data.placeOfBirth}
        poduims={data.podiums}
        points={data.totalPoints}
        teamName={data.teamName}
        pictureHelmetUrl={data.pictureHelmetUrl}
        worldChampionchip={data.worldChampionships}
        colorHex={data.colorHex}
      />
      }
    </div>
  );
};

export default Driver;

import React, { useState } from "react";
import HeroSlider from "../Components/Dashboard/HeroSlider";
import BottomSlider from "../Components/Dashboard/BottomSlider";
import CircuitMap from "../Components/Dashboard/DashboardBody/CircuitMap";
import Drivers from "../Components/Dashboard/DashboardBody/Drivers";
import { useGetGrandPrixByName } from "../Api/GrandPrix/GrandPrixApi";

type Props = {};

const Dashboard = (props: Props) => {
  let [bottomSliderSelected, setBottomSliderSelected] = useState<boolean>(true);
  const { data, refetch, isLoading, isError, isRefetching } = useGetGrandPrixByName(
    "Monaco"
  );

  return (
    <div>
      <HeroSlider 
        endDate={data !== undefined ? data?.endDate : new Date()}
        grandPrixName={data !== undefined? data?.city : ""}
        startDate={data !== undefined ? data?.startDate : new Date()}
      />
      <BottomSlider
        bottomSliderSelected={bottomSliderSelected}
        setBottomSliderSelected={setBottomSliderSelected}
      />
      { bottomSliderSelected ? 
      <CircuitMap 
        additionalInformation={data !== undefined ? data?.additionalInformation : "An unexpected error occurred!"}
        circuitLength={data !== undefined ? data?.circuitLength : 0}
        firstGrandPrix={data !== undefined ? data?.firstGrandPrix : 0}
        lapRecord={data !== undefined ? data?.lapRecord : "N/A"}
        lapRecordDriver={data !== undefined ? data?.lapRecordDriver : "N/A"}
        numberOfLaps={data !== undefined ? data?.numberOfLaps : 0}
        raceDistance={data !== undefined ? data?.raceDistance : 0}
      /> 
      : <Drivers 
        grandPrixId={data !== undefined ? data?.grandPrixId : 0}
      />}
    </div>
  );
};

export default Dashboard;

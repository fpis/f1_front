import React, { useEffect, useState } from "react";
import DiscountBanner from "../Components/BookReservation/DiscountBanner";
import "./BookReservation.scss";
import Filters from "../Components/BookReservation/Filters";
import FilterDays from "../Components/BookReservation/FilterDays";
import MonacoSeats from "../Assets/monaco-seats.png";
import ReservationSection from "../Components/BookReservation/ReservationSection";
import CartItems from "../Components/BookReservation/Cart/CartItems";
import CustomerInfo from "../Components/BookReservation/Cart/CustomerInfo";
import { useGetTicketsFiltered } from "../Api/Ticket/TicketApi";
import { TicketFiltersModel, TicketModel } from "../Api/Ticket/TicketApi.types";
import { MakeReservationModel, ReservationItemModel, MakeReservationItemModel } from "../Api/Reservation/ReservationApi.types";
import { useMakeReservation } from "../Api/Reservation/ReservationApi";

type Props = {};

const BookReservation = (props: Props) => {
  const [days, setDays] = useState<number[]>([1, 2, 3]);
  const [checkedDisabled, setCheckedDisabled] = useState(false);  
  const [checkedLargeTv, setCheckedLargeTv] = useState(true);
  const [value, setValue] = useState<number[]>([190, 8995]);
  const [minMaxValue, setMinMaxValue] = useState<number[]>([190, 8995]);
  const [reservationItems, setReservationItems] = useState<ReservationItemModel[]>([]);
  const [indicator, setIndicator] = useState(false);
  const [promoCodeDiscount, setPromoCodeDiscount] = useState<number>(0);
  const [verifyingPromoCode, setVerifyingPromoCode] = useState(false);
  const [totalPrice, setTotalPrice] = useState<number>(0);
  const [reservationObject, setReservationObject] = useState<MakeReservationModel>({
    address1: "",
    cityName: "",
    countryId: 0,
    email: "",
    firstName: "",
    lastName: "",
    postalCode: 0,
    reservationItems: []
  });

  const [filter, setFilter] = useState<TicketFiltersModel>({
    days: days,
    hasLargeTv: checkedLargeTv,
    highPrice: 0,
    lowPrice: 0,
    suitableForDisabled: checkedDisabled
  });
  const { data: reservationData, refetch: reservationRefetch, isError: reservationIsError, isLoading: reservationIsLoading } = useMakeReservation(reservationObject);
  const { data, isError, refetch, isRefetching, isLoading } = useGetTicketsFiltered(filter);

  useEffect(() => {
    refetch();
    console.log(data);
  }, [filter, setFilter]);

  useEffect(() => {
    if(reservationObject.reservationItems.length === 0 && indicator){
      alert("There is no items in cart!")
    }
    else if(indicator){
      reservationRefetch();
      setReservationItems([]);
    }
  }, [reservationObject, setReservationObject])

  useEffect(() => {
    if(verifyingPromoCode){
      setPromoCodeDiscount(0.05 * totalPrice)
    }
  }, [totalPrice, setTotalPrice])

  return (
    <div>
      <DiscountBanner />
      <hr />
      {data !== undefined ? 
        <Filters
          setFilters={
            () => {
              setFilter(() => {
                return{
                  days: days,
                  hasLargeTv: checkedLargeTv,
                  highPrice: value[1],
                  lowPrice: value[0],
                  suitableForDisabled: checkedDisabled
                }
              })
            }
          }
          checkedDisabled={checkedDisabled}
          checkedLargeTv={checkedLargeTv}
          minMaxValue={minMaxValue}
          setCheckedDisabled={setCheckedDisabled}
          setCheckedLargeTv={setCheckedLargeTv}
          setMinMacValue={setMinMaxValue}
          setSliderValue={setValue}
          sliderValue={value}
        />
        :<div></div>
      }
      <div className="main-book-reservation">
        <FilterDays 
          setDays={setDays}/>
        <img src={MonacoSeats} alt="monaco-seats" width={400} />
        <ReservationSection
          data={data}
          reservationItems={reservationItems}
          setReservationItems={setReservationItems}
        />
      </div>
      <div className="cart-book-reservation">
        <CartItems
          setTotalPrice={setTotalPrice}
          totalPrice={totalPrice}
          promoCodeDiscount={promoCodeDiscount}
          setPromoCodeDiscount={setPromoCodeDiscount} 
          items={reservationItems} 
          setItems={setReservationItems}/>
        <CustomerInfo
          totalPrice={totalPrice}
          reservationIsError={reservationIsError}
          setVerifyingPromoCode={setVerifyingPromoCode}
          verifyingPromoCode={verifyingPromoCode}
          reservationIsLoading={reservationIsLoading}
          setPromoCodeDiscount={setPromoCodeDiscount}
          reservationData={reservationData}
          reservationRefetch={reservationRefetch}
          response={reservationData ? reservationData : undefined}
          setReservationObject={setReservationObject}
          handleSendReservation={() => {
            const items: MakeReservationItemModel[] = reservationItems.map((ri) => {
              return {
                quantity: ri.quantity,
                raceIds: ri.raceIds,
                zoneId: ri.zoneId
              }
            });
          setReservationObject(prev => ({
            ...prev,
            reservationItems: items
          }));
          setIndicator(true);
        }}/>
      </div>
    </div>
  );
};

export default BookReservation;
